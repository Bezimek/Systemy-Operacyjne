from pages_processes import page_replacement as pr
from pages_processes import process_ordering as po


results = []                                                                                # Declaration of list with results for particular frame numbers 
R = (3,5,7)                                                                                 # Three example numbers of frames
for y in R:                                                                                 # Main loop checking both algorithms for all numbers of frames
    sum1, sum2 = 0, 0                                                                       # Variables summing up results
    for i in range(100):                                                                    # Secondary loop for the specified number of frames
        x = pr.PageReplacement.generator()
        sum1 += pr.PageReplacement.algorithm_FIFO(x, y)                                    # Calling FIFO algorithm and summing up its results to the sum1
        sum2 += pr.PageReplacement.algorithm_LRU(x, y)                                     # Calling LRU algorithm and summing up its results to the sum2
        #print("=================")                                                         # Remove comment in case if you uncommented "print" line in module
    results.append((sum1/100, sum2/100))                                                    # Adding to the results list average replaced amounts

for i,j in enumerate(results):                                                              # Loop showing results for all frames
        print("FIFO", "%.2f" % (j[0]), "-", "%.2f" % (j[1]), "LRU", "with", R[i], "frames")
input("Press ENTER to continue...")

print("<|========================================|>")

sum1, sum2, sum3, sum4 = 0,0,0,0
for i in range(100):                                                                        # Main loop initating the algorithms and summing them up
    x = po.ProcessOrdering.generator()
    sum1 += po.ProcessOrdering.algorithm_SJF(x)[0]                                         # Calling SJF algorithm and summing up its results to the sum1
    sum2 += po.ProcessOrdering.algorithm_SJF(x)[1]                                         # Calling SJF algorithm and summing up its results to the sum2
    sum3 += po.ProcessOrdering.algorithm_FCFS(x)[0]                                        # Calling FCFS algorithm and summing up its results to the sum3
    sum4 += po.ProcessOrdering.algorithm_FCFS(x)[1]                                        # Calling FCFS algorithm and summing up its results to the sum4
    #print("=============================================")                                 # Remove comment, in case if you uncommented "print" line in module
print("|SJF| Average waiting time:", "%.3f" % (sum1/100), "Average processing time:", "%.3f" % (sum2/100))
print("|FCFS| Average waiting time:", "%.3f" % (sum3/100), "Average processing time:", "%.3f" % (sum4/100))
print(2 * "==========================================")
input("Press ENTER to continue...")
