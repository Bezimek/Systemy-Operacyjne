import szeregowanie_procesów as SP

data = []                                                                                   # Lista zczytująca oraz przechowująca dane z pliku
file = open("processes_data.txt", "r")                                                      # Otwarcie pliku z danymi wejściowymi
for i in range(100):                                                                        # Pętla w której dane z pliku wejściowego są rozszyfrowywane oraz przekształcane na listy list
    dataline = [x.split(":") for x in file.readline().split(";")]                           # Rozdzielenie danych względem ":" oraz ";"
    dataline.pop()                                                                          # Usunięcie ostatniego elementu listy którym jest znak nowej linii
    for x in dataline:
            x[0],x[1] = int(x[0]), int(x[1])                                                # Przekształcenie typu danej ze string na integer
    data.append(dataline)                                                                   # Dodanie listy procesów do listy danych
file.close()                                                                                # Zamknięcie pliku
suma1, suma2, suma3, suma4 = 0,0,0,0
#file = open("processes_results.txt", "a")                                                  # Otwarcie pliku przechowującego wyniki
for i in data:                                                                              # Główna pętla wywołująca algorytmy oraz przekazująca iterator jako argument
    suma1 += SP.algorithm_SJF(i)[0]                                                         # Wywołanie funkcji z algorytmem SJF oraz zsumowanie jego wyniku do zmiennej (w miejsce "i" wpisać SP.generator() jeśli chcemy przetestować dla różnych wartości)
    suma2 += SP.algorithm_SJF(i)[1]                                                         # Wywołanie funkcji z algorytmem SJF oraz zsumowanie jego wyniku do zmiennej (w miejsce "i" wpisać SP.generator() jeśli chcemy przetestować dla różnych wartości)
    suma3 += SP.algorithm_FCFS(i)[0]                                                        # Wywołanie funkcji z algorytmem FCFS oraz zsumowanie jego wyniku do zmiennej (w miejsce "i" wpisać SP.generator() jeśli chcemy przetestować dla różnych wartości)
    suma4 += SP.algorithm_FCFS(i)[1]                                                        # Wywołanie funkcji z algorytmem FCFS oraz zsumowanie jego wyniku do zmiennej (w miejsce "i" wpisać SP.generator() jeśli chcemy przetestować dla różnych wartości)
    #file.write(4 * " " + str(algorithm_SJF(i)[0]) + 16 * " " + str(algorithm_SJF(i)[1]) + 14 * " " + str(algorithm_FCFS(i)[0]) + 16 * " " + str(algorithm_FCFS(i)[1]) + "\n")     # Zapis wyników do pliku
    #print("=============================================")                                 # Odkomentować, jeśli odkomentujemy wyświetlanie wyników każdej iteracji w module
#file.close()                                                                               # Zamknięcie pliku przechowującego wyniki
print("|SJF| Średni czas oczekiwania:", "%.3f" % (suma1/100), "Średni czas przetwarzania:", "%.3f" % (suma2/100))
print("|FCFS| Średni czas oczekiwania:", "%.3f" % (suma3/100), "Średni czas przetwarzania:", "%.3f" % (suma4/100))
print(2 * "==========================================")
input("Press ENTER to continue...")
