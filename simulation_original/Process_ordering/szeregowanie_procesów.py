import random

## Funkcja generator() odpowiada za generowanie ciągów procesów o danej składnii: ( <czas_przyjścia>, <czas_wykonania>, <identyfikator> ), wykorzystano do stworzenia pliku "data.txt"
def generator():
    #raw_data = ""
    tmp_list = []
    for i in range(100):
        tmp_list.append((random.randint(0, 100), random.randint(1, 20) , f"id-{i}"))                               # tmp_list zawiera elementy do wykorzystania przez program (wykorzystane przy testach)
        #raw_data += str(random.randint(0, 100)) + ":" + str(random.randint(1, 20)) + ":" +  str(f"id-{i}") + ";"  # raw_data zawiera dane w formie czas_przyjścia:czas_wykonania:identyfikator; następnie zapisane w pliku z danymi wejściowymi "data.txt"
    return tmp_list

## Implementacja algorytmu FCFS, który za argument przyjmuje listę krotek, wygenerowanych uprzednio za pomocą funkcji generator()
def algorithm_FCFS(processes):
    final_processes = sorted(processes, key = lambda x: x[0])                               # Posortowanie listy procesów względem czasu przyjścia
    processing_time = final_processes[0][0]                                                 # Zmienna odpowiadająca za czas od początku działania procesora
    processes_times = []                                                                    # Lista czasów oczekiwania procesów
    sum = 0                                                                                 # Zmienna do późniejszego zsumowania czasów oczekiwania procesów
    index = 0                                                                               # Zmienna przechowująca indeks tablicy
    while True:                                                                             # Pętla której zadaniem jest obliczanie mijającego czasu oraz notowanie poszczególnych czasów oczekiwania danych procesów
        try:
            if final_processes[index][0] <= processing_time:                                # Jeśli proces jest w stanie gotowości, należy zsumować go do czasu przetwarzania oraz dodać jego czas oczekiwania do tablicy processes_times
                processes_times.append(processing_time - final_processes[index][0])         # Dodanie czasu oczekiwania procesu do tablicy ( z uwzględnieniem jego czasu przybycia)
                processing_time += final_processes[index][1]                                # Zwiększenie czasu przetwarzania o czas wykonywania procesu
                index += 1                                                                  # Zwiększenie indeksu tablicy o 1
            elif final_processes[index][0] > processing_time:                               # Jeśli żaden proces nie jest w stanie gotowości, należy zwiększyć czas przetwarzania o 1
                processing_time += 1
        except IndexError:                                                                  # Obsługa błędu, gdy indeks wykroczy poza zakres tablicy
            break

    #print("Algorytm FCFS:")                                                                # Wyświetlenie nazwy algorytmu (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    for i in processes_times:                                                               # Sumowanie czasów oczekiwania
        sum += i
    #print("Sredni czas oczekiwania na procesor: %.3f" % (sum/len(final_processes)))        # Wyświetlanie średniego czasu oczekiwania na procesor (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    sum1 = sum                                                                              # Zmienna pomocnicza do sumowania dla czasów przetwarzania
    for i in final_processes:
        sum1 += i[1]                                                                        # Sumowanie czasów przetwarzania
    #print("Sredni czas cyklu przetwarzania: %.3f" % (sum1 / len(final_processes)))         # Wyświetlanie średniego czasu przetwarzania procesów (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    return (sum/len(final_processes), sum1 / len(final_processes))                          # Program zwraca krotkę z średnim czasem oczekiwania oraz średnim czasem przetwarzania

## Implementacja algorytmu SJF, który za argument przyjmuje listę krotek, wygenerowanych uprzednio za pomocą funkcji generator()
def algorithm_SJF(processes):
    processing_time = 0                                                                     # Zmienna odpowiadająca za czas od początku działania procesora
    processes_times = []                                                                    # Lista czasów oczekiwania procesów
    sum = 0                                                                                 # Zmienna do późniejszego zsumowania czasów oczekiwania procesów
    final_processes = []                                                                    # Posortowana lista procesów wg. kolejności ich wykonania

    while len(final_processes) != len(processes):                                           # Nieskończona pętla, która przerywana jest dopiero wtedy, gdy posortowana lista, będzie posiadać tyle procesów ile otrzymała funkcja
        queue = []                                                                          # Zmienna przechowująca elementy, które są w stanie gotowości w danym wykonaniu pętli
        for process in processes:                                                           # Iteracja po procesach aby sprawdzać każdy proces z osobna
            if process[0] <= processing_time:                                               # Warunek sprawdzający czy proces w danej iteracji jest w stanie gotowości
                if process in final_processes:                                              # "Odsiewanie" procesów wykonanych od procesów gotowych
                    continue
                queue.append(process)                                                       # Dodawanie procesów oczekujących na wykonanie
        if not queue:                                                                       # Jeśli żaden proces nie jest gotowy, to czas działania procesora zostaje zwiększony o 1, a pętla pomijana
            processing_time += 1
            continue

        queue.sort(key = lambda x: x[1])                                                    # Posortowanie gotowych procesów względem czasu ich wykonania
        final_processes.append(queue[0])                                                    # Dodanie do procesów końcowych obecnie wykonywanego procesu
        processes_times.append(processing_time - queue[0][0])                               # Dodanie czasu oczekiwania do listy czasów oczekiwania
        processing_time += queue[0][1]                                                      # Dodanie czasu wykonywania do czasu działania procesora

    #print("Algorytm SJF:")                                                                 # Wyświetlenie nazwy algorytmu (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    for i in processes_times:                                                               # Sumowanie czasów oczekiwania
        sum += i
    #print("Sredni czas oczekiwania na procesor: %.3f" % (sum/len(final_processes)))        # Wyświetlanie średniego czasu oczekiwania na procesor (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    sum1 = sum                                                                              # Zmienna pomocnicza do sumowania dla czasów przetwarzania
    for i in final_processes:
        sum1 += i[1]                                                                        # Sumowanie czasów przetwarzania
    #print("Sredni czas cyklu przetwarzania: %.3f" % (sum1 / len(final_processes)))         # Wyświetlanie średniego czasu przetwarzania procesów (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    return (sum / len(final_processes), sum1 / len(final_processes))                        # Program zwraca krotkę z średnim czasem oczekiwania oraz średnim czasem przetwarzania
