import zastepowanie_stron as ZS
import page_replacement as PR
import os 
dirname = os.path.dirname(__file__)
data = [] 
print(,__file__)                                                                            # Lista zczytująca oraz przechowująca dane z pliku
file = open(dirname + "/pages_data.txt", "r")                                               # Otwarcie pliku z danymi wejściowymi
for i in range(100):                                                                        # Pętla w której dane z pliku wejściowego są rozszyfrowywane oraz przekształcane na listy list
    dataline = [x for x in file.readline().split(":")]                                      # Rozdzielenie danych względem ":" oraz ";"
    dataline.pop()                                                                          # Usunięcie ostatniego elementu listy którym jest znak nowej linii
    for x,z in enumerate(dataline):
            dataline[x] = int(z)                                                            # Przekształcenie typu danej ze string na integer
    data.append(dataline)                                                                   # Dodanie listy stron do listy danych
file.close()                                                                                # Zamknięcie pliku
wyniki = []                                                                                 # Zadeklarowanie listy z wynikami przy poszczególnej liczbie ramek
R = (3,5,7)                                                                                 # Trzy różne liczby ramek do przetestowania algorytmów
#file = open("pages_results.txt", "a")                                                      # Otwarcie pliku przechowującego wyniki
for y in R:                                                                                 # Główna pętla wykorzystująca algorytmy dla każdej liczby ramek
    suma1, suma2 = 0, 0                                                                     # Zmienne sumujące wyniki algorytmów
    for i in data:                                                                          # Pętla drugiego rzędu do przetestowania algorytmów przy konkretnej liczbie ramek
        #file.write("  " + str(algorithm_FIFO(i, R[0])) + 6*" " + str(algorithm_FIFO(i, R[1])) + 6*" " + str(algorithm_FIFO(i, R[2])) + 15*" " + str(algorithm_LRU(i, R[0])) + 6*" " + str(algorithm_LRU(i, R[1])) + 6*" " + str(algorithm_LRU(i, R[2])) + "\n")   # Linijka służąca do zapisu wyników do pliku
        suma1 += ZS.algorithm_FIFO(i, y)                                                    # Wywołanie funkcji z algorytmem FIFO oraz zsumowanie jego wyniku do zmiennej (w miejsce "i" wpisać ZS.generator() jeśli chcemy przetestować dla różnych wartości)
        suma2 += ZS.algorithm_LRU(i, y)                                                     # Wywołanie funkcji z algorytmem LRU oraz zsumowanie jego wyniku do zmiennej (w miejsce "i" wpisać ZS.generator() jeśli chcemy przetestować dla różnych wartości)
        #print("=================")                                                         # Odkomentować, jeśli odkomentujemy wyświetlanie wyników każdej iteracji w module
    wyniki.append((suma1/100, suma2/100))                                                   # Dodanie do listy wyników średniej ilości zastąpionych stron dla danej liczby ramek
#file.close()                                                                               # Zamknięcie pliku przechowującego wyniki
for i,j in enumerate(wyniki):                                                               # Pętla wyświetlająca uśrednione wyniki dla określonej liczby ramek
        print("FIFO", "%.2f" % (j[0]), "-", "%.2f" % (j[1]), "LRU", "przy liczbie ramek:", R[i])
input("Press ENTER to continue...")