import random

## Funkcja generator() odpowiada za generowanie ciągów stron
def generator():
    #raw_data = ""
    tmp_list = []
    for i in range(100):
        tmp_list.append(random.randint(0, 20))                                              # tmp_list do sprawdzania wyników dla losowych ciągów
        #raw_data += str(random.randint(0, 20)) + ":"                                       # raw_data służyło do zapisania danych do pliku w formacie numer_strony:numer_strony
    return tmp_list


## Implementacja algorytmu FIFO, który w argumentach przyjmuje listę stron oraz liczbę ramek
def algorithm_FIFO(pages, frames):
    counter = 0                                                                             # Zmienna sumująca liczbę zastąpień stron
    memory = []                                                                             # Lista zawierająca ramki
    for i in range(frames):                                                                 # Pętla, której zadaniem jest wypełnienie listy memory wartościami
        memory.append([-1, 50])                                                             # Symbolicznie -1 oznacza pustą ramkę, zaś 50 jest bezpieczną wartością "przebywania w pamięci" dzięki czemu ramka zostanie wypełniona jako pierwsza przed innymi już zajętymi ramkami
    for page in pages:                                                                      # Pętla iterująca po kolei po każdej stronie
        condition = False                                                                   # Zmienna warunkowa do sterowania programem
        for i in memory:                                                                    # Przejście po kolei po ramkach
            i[1] += 1                                                                       # Zwiększenie czasu przebywania w pamięci
            if page == i[0]:                                                                # Sprawdzenie czy strona istnieje w pamięci
                condition = True                                                            # Zmiana warunku na prawdziwy aby dokonać zakończenia danej iteracji pętli
        if condition:
            continue                                                                        # Pominięcie danej iteracji
        memory.sort(key = lambda x: x[1], reverse= True)                                    # W przypadku gdy strona nie znajdowała się w pamięci, wszystkie strony zostają posortowane względem czasu przebywania w pamięci
        for i in range(len(memory)):                                                        # Ponowne przejście po ramkach przy użyciu ich indeksów
            if i == 0:                                                                      # Dla pierwszego elementu w liscie (z najdłuższym czasem przebywania w pamięci) zostają wykonane poniższe intrukcje
                counter += 1                                                                # Ilość zastąpień zostaje zwiększona o jeden
                memory[i][0], memory[i][1] = page, 0                                        # Numer strony poprzedniej, zostaje zastąpiony nowym numerem strony, a czas przebywania w pamięci zostaje wyzerowany
    #print("Liczba zastąpionych stron FIFO:", counter)                                      # Wyświetlenie liczby zmian do konsoli (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    return counter                                                                          # Zwracana jest liczba zastąpień stron

## Implementacja algorytmu LRU, który w argumentach przyjmuje listę stron oraz liczbę ramek
def algorithm_LRU(pages, frames):
    counter = 0                                                                             # Zmienna sumująca liczbę zastąpień stron
    memory = []                                                                             # Lista zawierająca ramki
    for i in range(frames):                                                                 # Pętla, której zadaniem jest wypełnienie listy memory wartościami
        memory.append([-1, 50])                                                             # Symbolicznie -1 oznacza pustą ramkę, zaś 50 jest bezpieczną wartością "przebywania w pamięci" dzięki czemu ramka zostanie wypełniona jako pierwsza przed innymi już zajętymi ramkami
    for page in pages:                                                                      # Pętla iterująca po kolei po każdej stronie
        condition = False                                                                   # Zmienna warunkowa do sterowania programem
        for i in memory:                                                                    # Przejście po kolei po ramkach
            i[1] += 1                                                                       # Zwiększenie czasu przebywania w pamięci dla wszystkich stron
            if page == i[0]:                                                                # Sprawdzenie czy strona istnieje w pamięci
                i[1] = 0                                                                    # Wyzerowanie czasu przebywania dla powtórzonej strony
                condition = True                                                            # Zmiana warunku na prawdziwy aby dokonać zakończenia danej iteracji pętli
        if condition:
            continue                                                                        # Pominięcie danej iteracji
        memory.sort(key = lambda x: x[1], reverse= True)                                    # W przypadku gdy strona nie znajdowała się w pamięci, wszystkie strony zostają posortowane względem czasu przebywania w pamięci
        for i in range(len(memory)):                                                        # Ponowne przejście po ramkach przy użyciu ich indeksów
            if i == 0:                                                                      # Dla pierwszego elementu w liscie (z najdłuższym czasem przebywania w pamięci) zostają wykonane poniższe intrukcje
                counter += 1                                                                # Ilość zastąpień zostaje zwiększona o jeden
                memory[i][0], memory[i][1] = page, 0                                        # Numer strony poprzedniej, zostaje zastąpiony nowym numerem strony, a czas przebywania w pamięci zostaje wyzerowany
    #print("Liczba zastąpionych stron LRU :", counter)                                      # Wyświetlenie liczby zmian do konsoli (odkomentować, jeśli użytkownik chce widzieć wyniki w każdej iteracji)
    return counter                                                                          # Zwracana jest liczba zastąpień stron
