import random

class ProcessOrdering:
    ##  Generator() is responsible for creating processes lists in form like this: ( <arrival_time>, <execute_time>, <id> )
    @staticmethod
    def generator():
        tmp_list = []
        for i in range(100):
            tmp_list.append((random.randint(0, 100), random.randint(1, 20) , f"id-{i}"))        # tmp_list contains elements used with testing on random values
        return tmp_list
    
    ## SJF implementation, where an argument is a list of tuples
    def algorithm_SJF(processes):
        processing_time = 0                                                                     # Variable containing processor's running time
        processes_times = []                                                                    # List of processor waiting times
        sum0 = 0                                                                                # Variable summing up waiting times
        final_processes = []                                                                    # List of processes sorted through their execution time

        while len(final_processes) != len(processes):                                           # Infinite loop which won't end until list of sorted processes is the same length as the initial list 
            queue = []                                                                          # List containing processes, which are ready to be executed
            for process in processes:                                                           # Iteration through processes to check whether they are ready to be executed
                if process[0] <= processing_time:                                               # if instruction checking the "ready" state of the process
                    if process in final_processes:                                              # Removing "finished" processes from "ready and unfinished"
                        continue
                    queue.append(process)                                                       # Adding processes awaiting for execution
            if not queue:                                                                       # In case if no process is ready to be executed, loop is skipped and processor's running time incremented
                processing_time += 1
                continue

            queue.sort(key = lambda x: x[1])                                                    # Sorting ready processes by their execution time
            final_processes.append(queue[0])                                                    # Adding to final processes the current executed process
            processes_times.append(processing_time - queue[0][0])                               # Adding waiting time to the processor running time
            processing_time += queue[0][1]                                                      # Adding execution time to the processor running time

        #print("Algorytm SJF:")                                                                 # Print algorithm's name (remove comment in case if we wanna see results in each iteration)
        for i in processes_times:                                                               # Summing up waiting times
            sum0 += i
        #print("Sredni czas oczekiwania na procesor: %.3f" % (sum0/len(final_processes)))       # Print average waiting time (remove comment in case if we wanna see results in each iteration)
        sum1 = sum0                                                                             # Extra variable which purpose is to manage average execution time
        for i in final_processes:
            sum1 += i[1]                                                                        # Summing up execution times
        #print("Sredni czas cyklu przetwarzania: %.3f" % (sum1 / len(final_processes)))         # Print average execution time (remove comment in case if we wanna see results in each iteration)
        return (sum0 / len(final_processes), sum1 / len(final_processes))                       # Program returns tuple containing average waiting and execution times

    ##  FCFS implementation, where an argument is a list of tuples
    def algorithm_FCFS(processes):
        final_processes = sorted(processes, key = lambda x: x[0])                               # Sorting the list by processes arrival time
        processing_time = final_processes[0][0]                                                 # Variable containing processor's running time
        processes_times = []                                                                    # List of processor waiting times
        sum0 = 0                                                                                # Variable summing up waiting times
        index = 0                                                                               # Variable holding list index
        while True:                                                                             # Loop which is calculating time that has passed from the beginning and collecting particular processess waiting times
            try:
                if final_processes[index][0] <= processing_time:                                # If process is ready, it's execution time will be added to general execution time and his waiting time will be added to processes_times
                    processes_times.append(processing_time - final_processes[index][0])         # Adding process's waiting time to the list ( while paying attention to his arrivel time)
                    processing_time += final_processes[index][1]                                # Adding process's execution time to the general execution time
                    index += 1                                                                  # Incrementing list index by 1
                elif final_processes[index][0] > processing_time:                               # In case if no process is ready to be executed, loop is skipped and processor's running time incremented
                    processing_time += 1
            except IndexError:                                                                  # If index goes out of range, then the exception is handled
                break

        #print("Algorytm FCFS:")                                                                # Print algorithm's name (remove comment in case if we wanna see results in each iteration)
        for i in processes_times:                                                               # Summing up waiting times
            sum0 += i
        #print("Sredni czas oczekiwania na procesor: %.3f" % (sum/len(final_processes)))        # Print average waiting time (remove comment in case if we wanna see results in each iteration)
        sum1 = sum0                                                                             # Extra variable which purpose is to manage average execution time
        for i in final_processes:
            sum1 += i[1]                                                                        # Summing up execution times
        #print("Sredni czas cyklu przetwarzania: %.3f" % (sum1 / len(final_processes)))         # Print average execution time (remove comment in case if we wanna see results in each iteration)
        return (sum0/len(final_processes), sum1 / len(final_processes))                         # Program returns tuple containing average waiting and execution times


