import random

class PageReplacement:

    @staticmethod
    def generator():
        tmp_list = []
        for i in range(100):
            tmp_list.append(random.randint(0, 20))                                              # tmp_list is used to check results for random values
        return tmp_list

    ## LRU algorithm implementation, which requires pages numbers and number of frames
    def algorithm_LRU(pages, frames):
        counter = 0                                                                             # Variable summing up how many replaces occured
        memory = []                                                                             # List containing frames
        for i in range(frames):                                                                 # Loop which is supposed to fill up the frames
            memory.append([-1, 50])                                                             # Value -1 marks "empty" frames and value 50 is a safe "memory lifetime value" thanks to which empty frame is filled before full frames
        for page in pages:                                                                      # Loop iterating after each page
            condition = False                                                                   # Boolean controlling flow
            for i in memory:                                                                    # Looping through frames
                i[1] += 1                                                                       # Incrementation of "memory lifetime value" for non changed pages in frames
                if page == i[0]:                                                                # Check if page already is contained by any of frames
                    i[1] = 0                                                                    # If the page is repeated, its "memory lifetime value" is set again to 0, so the website that is used more often, will stay in memory longer.
                    condition = True                                                            # Change boolean to TRUE so it can skip the current loop
            if condition:
                continue                                                                        # Skipping the loop
            memory.sort(key = lambda x: x[1], reverse= True)                                    # In case if page wasn't detected in memory, all pages will be sorted via "memory lifetime value"
            for i in range(len(memory)):                                                        # Looping again through frames, this time using their indexes
                if i == 0:                                                                      # For the first element in the list (the one that stayed in memory for the longest period of time) will be occured the following instructions
                    counter += 1                                                                # Replaces counter is incremented
                    memory[i][0], memory[i][1] = page, 0                                        # Previous page is replaced with new one, and "memory lifetime value" is set to 0
        #print("Replaced pages in FIFO:", counter)                                              # Prints out the replace counter to the console 
        return counter                                                                          # Method returns the value of how many replaces occured
   
    ## FIFO algorithm implementation, which requires pages numbers and number of frames
    def algorithm_FIFO(pages, frames):
        counter = 0                                                                             # Variable summing up how many replaces occured
        memory = []                                                                             # List containing frames
        for i in range(frames):                                                                 # Loop which is supposed to fill up the frames
                memory.append([-1, 50])                                                         # Value -1 marks "empty" frames and value 50 is a safe "memory lifetime value" thanks to which empty frame is filled before full frames
        for page in pages:                                                                      # Loop iterating after each page
            condition = False                                                                   # Boolean controlling flow
            for i in memory:                                                                    # Looping through frames
                i[1] += 1                                                                       # Incrementation of "memory lifetime value" for non changed pages in frames
                if page == i[0]:                                                                # Check if page already is contained by any of frames
                    condition = True                                                            # Change boolean to TRUE so it can skip the current loop
            if condition:
                continue                                                                        # Skipping the loop
            memory.sort(key = lambda x: x[1], reverse= True)                                    # In case if page wasn't detected in memory, all pages will be sorted via "memory lifetime value"
            for i in range(len(memory)):                                                        # Looping again through frames, this time using their indexes
                if i == 0:                                                                      # For the first element in the list (the one that stayed in memory for the longest period of time) will be occured the following instructions
                    counter += 1                                                                # Replaces counter is incremented
                    memory[i][0], memory[i][1] = page, 0                                        # Previous page is replaced with new one, and "memory lifetime value" is set to 0
        #print("Replaced pages in FIFO:", counter)                                              # Prints out the replace counter to the console 
        return counter                                                                          # Method returns the value of how many replaces occured
