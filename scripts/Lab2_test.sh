#!/bin/bash

#Z1
mkdir -p $1

#Z2
cd $1
mkdir D1 D2 D3 D4
touch D2/F1.txt D3/F1.txt D4/F1.txt
cd -

#Z3
ln -s /etc/passwd $1/D1/

#Z4
readlink $1/D1/passwd

#Z5
ln $1/D3/F1.txt $1/D2/F2.txt

#Z6
chmod 600 $1/D2/F2.txt

#Z7
sudo chown :users $1/D2/F2.txt

#Z8
sudo chgrp users $1/D3

#Z9
chmod a-x $1/D3

#Z10
chmod a-w $1/D2

#Z11
chmod a-r $1/D4

#Z12
chmod o+t $1/D4

#Z13
touch $1/D1/scr1.sh
chmod ug+x $1/D1/scr1.sh

#Z14
chmod +s+g $1/D1/scr1.sh
