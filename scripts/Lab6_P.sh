#!/bin/bash

#Z1
basic_operations(){
  if (($1 + $2 == 0 )); then
    echo "Mianownik jest równy zero, przerywam program"
    exit -1
  fi
echo $[($1 - $2)/($1 + $2)]
return 0
}
basic_operations 15 -25

#Z2
recursive_fibonacci(){
  if (( $# != 1 )); then
    echo "Niewlasciwa liczba argumentow, przerywam program"
    exit -1
  fi

  if (( $1 <= 0 )); then
    echo 0
  elif (( $1 == 1 )); then
    echo 1
  else
    echo $((`recursive_fibonacci $[$1 - 2]` + `recursive_fibonacci $[$1 - 1]`))
  fi
}
recursive_fibonacci 10

#Z3
iterative_fibonacci(){
  if (( $# != 1 )); then
    echo "Niewlasciwa liczba argumentow, przerywam program"
    exit -1
  fi
  x=0
  y=1

  for (( i=0; i<$1; i++))
  do
     fn=$((x+y))
     x=$y
     y=$fn
  done
  echo "$x"
}
iterative_fibonacci 10

#Z4

find_maximum(){
  numbers=("$@")
  MAX=${numbers[0]}
  for i in ${numbers[*]}; do
    if (( $(echo "$MAX < $i" | bc -l))); then
      MAX=$i
    fi
  done
  echo $MAX
}
find_maximum 10 12 5 3 200 200.5 5 123
