#!/bin/bash

#Z1
basic_operations(){
  if (( $1 - $2 == 0 )); then
    echo "Mianownik jest równy zero, przerywam program"
    exit -1
  fi
echo $[($1 + $2)/($1 - $2)]
return 0
}
basic_operations 15 25

#Z2
recursive_factorial(){
  if (( $# != 1 )); then
    echo "Niewlasciwa liczba argumentow, przerywam program"
    exit -1
  fi
  if (($1 <= 1)); then
      echo 1
  else
    echo $((`recursive_factorial $[$1 - 1]` * $1))
  fi
}
recursive_factorial 5

#Z3
iterative_factorial(){
  if (( $# != 1 )); then
    echo "Niewlasciwa liczba argumentow, przerywam program"
    exit -1
  fi
  total=1
  for(( i=1; i<=$1; i++ )); do
    total=$(($total * $i))
  done
  echo $total
}
iterative_factorial 5

#Z4
sum_them_all(){
 numbers=("$@")
 total=0
 for i in ${numbers[*]}; do
   total=$(($i + $total))
 done
 echo $total
}
sum_them_all 2 5 10
