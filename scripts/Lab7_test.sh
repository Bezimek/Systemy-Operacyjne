#!/bin/bash

#Z1

bubble_sort(){

        local tablica=("$@")
        size=${#tablica[@]}

        for ((i = 0; i<size; i++)); do   
                for((j = 0; j<size-i-1; j++)); do
          if [[ "${tablica[j]}" -gt "${tablica[$((j+1))]}" ]]; then
                   tmp=${tablica[j]}  
                   tablica[$j]=${tablica[$((j+1))]}    
                   tablica[$((j+1))]=$tmp  
              fi
                done
        done
          
        echo ${tablica[*]}
    echo
  }
bubble_sort 23 2 10 1 5 8 91


#Z2

multiplication_arrays()
{
    if (( $# != 2 )); then
                echo "Niewlasciwy rozmiar tablicy, przerywam funkcje!"
                exit 1
        fi
    if (( $1 <= 0 || $2 <= 0)); then
      echo "Jeden lub oba argumenty sa niedodatnie, przerywam program"
      exit 1
    fi

        declare -A tablica
    for ((i=0; i<$1; i++)); do
      for ((j=0; j<$2; j++)); do
          tablica[i,j]=$(($i * $j))
          echo -n "$(($i * $j)) " 
                done
        echo " "
        done
    #echo ${tablica[*]}
    echo
}
multiplication_arrays 11 11


#Z3

numerical_integration(){

        local array=("$@")
        local count=${#array[@]}
        local total=0

        for ((i = 1; i<count-1; i++)); do   
                local val1=${array[i]}
                local val2=${array[i+1]}
                total=$(echo "$total + ($val1 + $val2) / 2" | bc -l)
        done

        local integral=$(echo "$total + (${array[0]} + ${array[$count-1]}) / 2" | bc -l)
        echo $integral
        echo
}
numerical_integration 1 2 3.5 10 25.75
