#!/bin/bash

#Z1
mkdir -p $1

#Z2
ln -s /etc/passwd $1

#Z3
wc -l $1/passwd

#Z4
du $1/passwd

#Z5
head -n 10 $1/passwd

#Z6
tail -n 5 $1/passwd

#Z7
cat /etc/group | grep $(groups $(whoami) | cut -d ' ' -f 4) | grep $(id -G | cut -d ' ' -f 2) |  cut -d ':' -f 4
#Z8
cat $1/passwd | cut -d ':' -f 1,7 | tr ":" "," | sort -d -t ',' -k2 > $1/F1.csv

#Z9
cat $1/passwd | cut -d ':' -f4 | sort -n | uniq > $1/F2.csv

#Z10
cat $1/passwd | grep -A 5 -B 4 root > $1/F4.csv

#Z11
diff $1/passwd $1/F4.txt

#Z12
dd bs=1 skip=10 count=20 if=$1/passwd of=$1/passwdPiece
