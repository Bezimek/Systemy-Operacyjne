#!/bin/bash

#Z1
mkdir -p $2

#Z2
find $1 -type f -iname *.csv

#Z3
find $1 -type d \( -name "A*" -o -name "B*" -o -name "c*" \)

#Z4
find $1 -type f -perm -u=rx

#Z5
find $1 -type f -empty

#Z6
find $1 -type l

#Z7
find $1 -type f -group users -size +100k \( -name "*.txt" -o -name "*.csv" \)

#Z8
find $1 -type f \( -perm -a=x,g=s -o -perm -a=x,u=s \)

#Z9
find $1 -type d -perm -o=t

#Z10
find $1 -type f -mtime -1

#Z11
find /dev -type b

#Z12
find $1 -type f,d -empty -exec rm -r {} ';'

#Z13
find $1 -type f -perm /a=x -name "*.sh" -exec mv {}  $2 ';'

#Z14
find $1 -type d -perm /a=r -exec cp -r {}/ $2 ';'
