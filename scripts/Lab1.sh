#!/bin/bash

#Z1
mkdir $1

#Z2
cd $1

#Z3
mkdir -p d1/d1.1 d2/d2.1 d3/d3.1 d3/d3.2 Aa Bb Cc
touch d1/t1.txt d2/d2.1/f1.csv d3/d3.2/f2.csv 

#Z4
mv d1/t1.txt d1/d1.1/t1.txt

#Z5
cp d2/d2.1/f1.csv d1/f1.csv

#Z6
mv Aa Aa1

#Z7
ls -d *1 */*1 *2 */*2

#Z8
du -h --max-depth=1
