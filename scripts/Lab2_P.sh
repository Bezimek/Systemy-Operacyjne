#/bin/bash

#Z1
mkdir -p $1

#Z2
cd $1
mkdir -p A1/A11 B1/B11 C
touch A1/t1.txt B1/B11/f1.csv C/ccc.txt
cd -

#Z3
ln -s /etc/passwd $1/A1/

#Z4
readlink $1/A1/passwd

#Z5
ln $1/B1/B11/f1.csv $1/B1/F2.txt

#Z6
chmod 600 $1/B1/F2.txt

#Z7
sudo chown :users $1/B1/F2.txt

#Z8
sudo chgrp users $1/C 

#Z9
chmod 666 $1/C

#Z10
chmod 555 $1/B1

#Z11
chmod 333 $1/A1

#Z12
chmod 1666 $1/C

#Z13
sudo touch $1/C/scr1.sh
sudo chmod 776 $1/C/scr1.sh

#Z14
sudo chmod 6776 $1/C/scr1.sh

#Z15
tree --inodes $1
