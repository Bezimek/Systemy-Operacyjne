#!/bin/bash

#Z1
rand_divider(){
if [[ ! -e $1 ]]; then
    echo "Plik wejsciowy $1 nie istnieje"
    exit -1 
fi
if [[ ! -r $1 ]]; then
    echo "Brak uprawnien do odczytu"
    exit -1
fi
if [[ -e $2 ]]; then
  if [[ -w $2 ]]; then
    echo "" > $2
  else
    echo "Brak prawa do zapisu dla pliku: $2"
    return -1
  fi
else
  touch $2
fi
if [[ -e $3 ]]; then
  if [[ -w $3 ]]; then
    echo "" > $3
  else
    echo "Brak prawa do zapisu dla pliku: $3"
    return -1
  fi
else
  touch $3
fi
i=1
RANDOM=$(date +%s%N | cut -b10-19) 
while read line; do
  if (($RANDOM%2==0)); then
    echo "$i $line" >> $2
  else
    echo "$i $line" >> $3
  fi
  i=$((i+1))
done < $1
}
#rand_divider $1 $2 $3

#Z2
dir_struct(){
  if [[ ! -d $1 ]]; then
    echo "Sciezka nie wskazuje na katalog!"
    return -1
  fi
  if [[ ! -w $1 ]]; then
    echo "Brak prawa do zapisu do katalogu: $1"
    return -1
  fi
  if [[ ! -f $2 ]]; then
    echo "Plik $2 nie istnieje!"
    return -1
  fi
  if [[ ! -r $2 ]]; then
    echo "Brak prawa do odczytu pliku: $2"
    return -1
  fi
  while read line; do
      mkdir -p "$1/$line"
      for i in {0..9}; do
        touch $1/$line/F$i
      done
  done < $2
}
#dir_struct $1 $2
