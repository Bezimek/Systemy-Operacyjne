#!/bin/bash

#Z1
insert_sort(){
    arr=( "$@" )
    n=${#arr[@]}  
    tmp=0

    for ((i = 0; i < n; i++)); do
        for ((j = i-1; j >= 0; j--)); do
          if (( $(echo "${arr[j]} > ${arr[$((j+1))]}" | bc -l) )); then
                tmp=${arr[j]}
                arr[j]=${arr[$((j+1))]}
                arr[$((j+1))]=$tmp
            fi
        done
    done

    echo "${arr[@]}"
}
insert_sort 1 2.5 1.2 80 23.25 24.10 23.40

#Z2
create_array(){
  if(($1 < 0 || $2 < 0)); then
    echo "Jedna lub obie zmienne przyjely wartosc ujemna, dzialanie zostaje przerwane!"
    exit 1
  fi
  declare -A double_array
  for ((i=0;i<$1;i++)); do
    for ((j=0;j<$2;j++)); do
      double_array[$i, $j]=$(($RANDOM%21))
    done
  done

  echo ${double_array[*]}

}
create_array 10 5
