#!/bin/bash
#Z1
grep -oP '(?<=^| )0x[a-fA-F\d]+(?=$| )' $1
#Z2
grep -oP '(?<=^| )[a-zA-Z\d.,;:]+@[a-zA-Z\d-]+\.[a-zA-Z\d-.]+[a-zA-Z\d-](?=$| )' $1
#Z3
grep -oP '(?<=^| )[+-]{0,1}[\d]{0,}\.\d+(?=$| )' $1
