# Operating-Systems



> Contains bash scripts and simulation project 
> written for the Operating Systems classes.   


## Simulation project

 - The simulation project originally was written in Polish language for the OS classes at uni.
It has been modified and translated, so interesting files are main.py and the modules in Pages_Processes_OOP dir.

 - Though, original files has been preserved and hidden in the remaining folders, unfortunately they haven't been translated, so it is 
recommended to be familiar with Polish
